# english_words library (used to find words in the language):
from english_words import get_english_words_set
# Set with all words in the English dictionary:
en_us = get_english_words_set(["web2", "gcide"], lower=True)


# PyDictionary library (used to define words):
from PyDictionary import PyDictionary
# PyDictionary Object:
dictionary = PyDictionary()


# Standard key pad found in most cellphones
standard_keypad = {
    "1": None,
    "2": ["a", "b", "c"],
    "3": ["d", "e", "f"],
    "4": ["g", "h", "i"],
    "5": ["j", "k", "l"],
    "6": ["m", "n", "o"],
    "7": ["p", "q", "r", "s"],
    "8": ["t", "u", "v"],
    "9": ["x", "y", "w", "z"],
    "0": ["+"],
    "*": ["*"],
    "#": ["#"]
}

# Keypad used in the Alpha Omega level in Black Ops 4
bo4_keypad = {
    "1": ["q", "z"],
    "2": ["a", "b", "c"],
    "3": ["d", "e", "f"],
    "4": ["g", "h", "i"],
    "5": ["j", "k", "l"],
    "6": ["m", "n", "o"],
    "7": ["p", "r", "s"],
    "8": ["t", "u", "v"],
    "9": ["w", "x", "y"],
    "0": None
}


# numer_code	-->		String that represents the number that you want to decode
# keypad 		-->		HashMap that represents the keypad setup that you want to use (above we have two examples, a standard and the one found in Black Ops 4):
# 								The KEY of the HashMap is a String that represents the number on the keypad (ex. '1', '2', '3', etc...)
# 								The VALUE of the HashMap is a List of Strings that contains all the letters that can be used with that digit (ex. ['a','b','c'])
# language		-->		Set that represents all the possible words that you want to find (above we used the english_words library for this)
# dictionary	-->		PyDictionary Object that you want to use (above we used the PyDictionary library for this)
# definitions	-->		Boolean condition set to False by default. If set to True, the program will print the definitions of the words (if available)
# print_all		-->		Boolean condition set to False by default. If set to True, the program will print all possible combinations, not only the words found in the language
def possible_words(number_code, keypad, language, dictionary, definitions=False, print_all=False):
    line = "---------------------------------------------------------------------------------------------------------------------"
    print("{}\n- CODE: {}\n{}".format(line, number_code, line))

    def generate_words(curr_str, number, index):
        if index == len(number):
            # NOT 100% OPTIMIZED:
            if print_all:
                print("{}".format(curr_str))
                if not definitions:
                    return

            if curr_str not in language:
                return

            if not definitions:
                print("{}".format(curr_str))
                return

            if not print_all:
                print(curr_str)

            word_meaning = dictionary.meaning(curr_str)
            if not word_meaning:
                print('No definition found for "{}"'.format(curr_str))
                print()
                return

            for definitionType in word_meaning:
                i = 1
                word_definitions = dictionary.meaning(curr_str)[definitionType]
                if not word_definitions:
                    print('Unexpected error for word "{}"')
                    return

                for definition in word_definitions:
                    print("{} ({}): {}".format(definitionType, i, definition))
                    i += 1
            print()
            return

        digit = number[index]
        if digit in keypad:
            letters = keypad[digit]
            if letters:
                for letter in letters:
                    generate_words(curr_str + letter, number, index + 1)
            else:
                print(
                    'Error: "{}" does not have any corresponding letters'.format(digit)
                )
                input("Press ENTER to continue...\n")
                generate_words(curr_str, number, index + 1)
        else:
            print('Error: "{}" is not on the keypad'.format(digit))
            input("Press ENTER to continue...\n")
            generate_words(curr_str, number, index + 1)

    generate_words("", number_code, 0)
    print()


# # tests:

# # print('definitions = False, printAll = False')
# possible_words("3825", bo4_keypad, en_us, dictionary, False, False)

# # print('definitions = False, printAll = True')
# possible_words("3825", bo4_keypad, en_us, dictionary, False, True)

# # print('definitions = True, printAll = False')
# possible_words("3825", bo4_keypad, en_us, dictionary, True, False)

# # print('definitions = True, printAll = True')
# possible_words("3825", bo4_keypad, en_us, dictionary, True, True)



possible_words("3825", bo4_keypad, en_us, dictionary)

# possible_words("8463", standard_keypad, en_us, dictionary)

# possible_words("1234567890", standard_keypad, en_us, dictionary, False, True)
